<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class UserController extends Controller
{
    //
    public function index(){

    }
    public function registerEmployer(){

        return view('employer_register');

    }
    public function registerEmployerPost(Request $request){
        $rules = [
            'name'      => ['required', 'string', 'max:190'],
            'company'   => 'required',
            'email'     => ['required', 'string', 'email', 'max:190', 'unique:users'],
            'password'  => ['required', 'string', 'min:6', 'confirmed'],
            'phone'     => 'required',
            'address'   => 'required',
            'country'   => 'required',
            'state'     => 'required',
        ];
        $this->validate($request, $rules);

        $company = $request->company;
//        $company_slug = unique_slug($company, 'User', 'company_slug');

        $country = $request->country;
        $state_name = null;
        if ($request->state){
//            $state = State::find($request->state);
//            $state_name = $state->state_name;
        }

        User::create([
            'name'          => $request->name,
            'company'       => $company,
//            'company_slug'  => $company_slug,
            'email'         => $request->email,
            'user_type'     => 'employer',
            'password'      => Hash::make($request->password),

            'phone'         => $request->phone,
            'address'       => $request->address,
            'address_2'     => $request->address_2,
            'country_id'    => 1,
            'country_name'  => $country,
            'state_id'      => 2,
            'state_name'    => $state_name,
            'city'          => $request->city,
            'active_status' => 1,
        ]);
//    echo 'success';
        return redirect(route('login'))->with('success', __('app.registration_successful'));
    }

    public function registerWorker(){

        return view('worker_register');
    }
    public function registerWorkerPost(Request $request){
        $rules = [
            'name' => ['required', 'string', 'max:190'],
            'email' => ['required', 'string', 'email', 'max:190', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ];

        $this->validate($request, $rules);

        $data = $request->input();
        User::create([
            'name'          => $data['name'],
            'email'         => $data['email'],
            'user_type'     => 'user',
            'password'      => Hash::make($data['password']),
            'active_status' => 1,
        ]);

        return redirect(route('login'))->with('success', __('app.registration_successful'));
    }
    public function appliedJobs(){
        return view('admin.applied_jobs');
    }
    public function profile(){
        return view('admin.profile');
    }
    public function changePassword(){
        return view('admin.change_password');
    }

}
