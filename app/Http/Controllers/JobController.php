<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs;
use Illuminate\Support\Facades\Auth;
use App\User;
class JobController extends Controller
{
    //
    public function newJob(){
        return view('admin.post-new-job');
    }
    public function newJobPost(Request $request){
        $user_id = Auth::user()->id;
        $rules = [
            'job_title' => ['required', 'string', 'max:190'],
            'position' => ['required', 'string', 'max:190'],
            'category' => 'required',
            'description' => 'required',
            'deadline' => 'required',
        ];
        $this->validate($request, $rules);

        $job_title = $request->job_title;
        $job_slug = 'job_slug';


        $country = $request->country;
        $state_name = null;
        if ($request->state){
            $state = State::find($request->state);
            $state_name = $state->state_name;
        }

        $job_id = strtoupper(str_random(8));
        $data = [
            'user_id'                   => $user_id,
            'job_title'                 => $job_title,
            'job_slug'                  => $job_slug,
            'position'                  => $request->position,
            'category_id'               => $request->category,
            'is_any_where'              => $request->is_any_where,
            'salary'                    => $request->salary,
            'salary_upto'               => $request->salary_upto,
            'is_negotiable'             => $request->is_negotiable,
            'salary_currency'           => $request->salary_currency,
            'salary_cycle'              => $request->salary_cycle,
            'vacancy'                   => $request->vacancy,
            'gender'                    => $request->gender,
            'exp_level'                 => $request->exp_level,
            'job_type'                => $request->job_type,

            'experience_required_years' => $request->experience_required_years,
            'experience_plus'           => $request->experience_plus,
            'description'               => $request->description,
            'skills'                    => $request->skills,
            'responsibilities'          => $request->responsibilities,
            'educational_requirements'  => $request->educational_requirements,
            'experience_requirements'   => $request->experience_requirements,
            'additional_requirements'   => $request->additional_requirements,
            'benefits'                  => $request->benefits,
            'apply_instruction'         => $request->apply_instruction,
            'country_id'                => $request->country,
            'country_name'              => $country,
            'state_id'                  => $request->state,
            'state_name'                => $state_name,
            'city_name'                 => $request->city_name,
            'deadline'                  => $request->deadline,
            'status'                    => 0,
            'is_premium'                => $request->is_premium,
        ];


        $job = Jobs::create($data);
        if ( ! $job){
            return back()->with('error', 'app.something_went_wrong')->withInput($request->input());
        }

        $job->update(['job_id' => $job->id.$job_id]);
        return redirect(route('posted_jobs'))->with('success', __('app.job_posted_success'));
    }
    public function postedJobs(){
        $title = __('app.posted_jobs');
        $user = Auth::user();
        $jobs = Jobs::paginate(2)->where('user_id', '=',$user->id);
        return view('admin.jobs', compact('title', 'jobs','user'));
    }
}
