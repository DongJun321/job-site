<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('new-register', 'HomeController@newRegister')->name('new_register');
//home page menus
Route::get('jobs','HomeController@jobs')->name('jobs');
Route::get('business', 'HomeController@business')->name('business');
Route::get('international', 'HomeController@international')->name('international');
Route::get('wages', 'HomeController@wages')->name('wages');
Route::get('tips', 'HomeController@tips')->name('tips');

//register employer and worker
Route::get('employer-register', 'UserController@registerEmployer')->name('employer');
Route::post('employer-register', 'UserController@registerEmployerPost');

Route::get('worker-register', 'UserController@registerWorker')->name('worker');
Route::post('worker-register', 'UserController@registerWorkerPost');


//dashboard routes
Route::group(['prefix'=>'dashboard', 'middleware' => 'dashboard'], function(){
    Route::get('/', 'DashboardController@dashboard')->name('dashboard');

    Route::group(['middleware'=>'employer_check'], function(){
        Route::group(['prefix'=>'employer'], function(){

            Route::group(['prefix'=>'job'], function(){
                Route::get('new', 'JobController@newJob')->name('post_new_job');
                Route::post('new', 'JobController@newJobPost');
                Route::get('edit/{job_id}', 'JobController@edit')->name('edit_job');
                Route::post('edit/{job_id}', 'JobController@update');
                Route::get('posted', 'JobController@postedJobs')->name('posted_jobs');
            });

            Route::get('applicant', 'UserController@employerApplicant')->name('employer_applicant');
            Route::get('shortlisted', 'UserController@shortlistedApplicant')->name('shortlisted_applicant');
            Route::get('applicant/{application_id}/shortlist', 'UserController@makeShortList')->name('make_short_list');

            Route::get('profile', 'UserController@employerProfile')->name('employer_profile');
            Route::post('profile', 'UserController@employerProfilePost');

        });
    });
    Route::group(['prefix'=>'u'], function(){
        Route::get('applied-jobs', 'UserController@appliedJobs')->name('applied_jobs');
        Route::get('profile', 'UserController@profile')->name('profile');
    });
    Route::group(['prefix'=>'payments'], function() {
        Route::get('/', 'PaymentController@index')->name('payments');
    });
    Route::group(['prefix' => 'account'], function() {
        Route::get('change-password', 'UserController@changePassword')->name('change_password');
    });

});


Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    return 'DONE'; //Return anything
});