<?php
/**
 * Created by PhpStorm.
 * User: a
 * Date: 4/8/2020
 * Time: 8:25 PM
 */
return [
    'employer_register'=>'Employer Register',
    'company'=>'Company Name',
    'phone'=>'Phone',
    'address'=>'Address',
    'address2'=>'Address2',
    'country_name'=>'Country',
    'state_name'=>'State',
    'city_name'=>'City',
    'worker_register'=>'Worker Register'
];