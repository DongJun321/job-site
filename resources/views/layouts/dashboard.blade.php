<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="#">

    <title>Dashboard</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{asset('assets/admin/css/app.css')}}" rel="stylesheet">
    <link href="{{asset('assets/admin/css/admin.css')}}" rel="stylesheet">


    <!-- Scripts -->
    <script src="{{asset('assets/admin/js/app.js')}}" defer></script>

</head>
<body>
@php
    $user = Auth::user();
@endphp
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container-fluid">
            <a class="navbar-brand" href="javascript:void(0);">
                <img src="https://hiljob.com/assets/images/logo.png" />
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item"><a class="nav-link" href=""><i class="la la-home"></i> View Site</a> </li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">

                    <li class="nav-item">
                        <a class="nav-link btn btn-success text-white" href="posted-new-job.html"><i class="la la-save"></i>Post new job </a>
                    </li>

                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{Auth::user()->name}}
                            <span class="badge badge-warning"><i class="la la-briefcase"></i>16</span>
                            <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>

                </ul>
            </div>
        </div>
    </nav>

    <div id="main-container" class="main-container">

        <div class="row">
            <div class="col-md-3">

                <div class="sidebar">
                    <ul class="sidebar-menu list-group">

                        <li class="active">
                            <a href="/dashboard" class="list-group-item-action active">
                                <span class="sidebar-icon"><i class="la la-home"></i> </span>
                                <span class="title">Dashboard</span>
                            </a>
                        </li>

                        <li class="">
                            <a href="{{route('applied_jobs')}}" class="list-group-item-action active">
                                <span class="sidebar-icon"><i class="la la-list-alt"></i> </span>
                                <span class="title">Applied Jobs</span>
                            </a>
                        </li>

                    @if($user->is_admin())
                        <li class="">
                            <a href="categories.html" class="list-group-item-action active">
                                <span class="sidebar-icon"><i class="la la-th-large"></i> </span>
                                <span class="title">Categories</span>
                            </a>
                        </li>
                    @endif

                    @if($user->is_employer())
                        <li class="">
                            <a href="#" class="list-group-item-action">
                                <span class="sidebar-icon"><i class="la la-black-tie"></i> </span>
                                <span class="title">Employer</span>
                                <span class="arrow"><i class="la la-arrow-right"></i> </span>
                            </a>

                            <ul class="dropdown-menu" style="display: none;">
                                <li><a class="sidebar-link" href="{{route('post_new_job')}}">Post new job</a></li>
                                <li><a class="sidebar-link" href="{{route('posted_jobs')}}">Posted Jobs</a></li>
                                <li><a class="sidebar-link" href="#">Applicants</a></li>
                                <li><a class="sidebar-link" href="#">Shortlist</a></li>
                            </ul>
                        </li>
                    @endif

                    @if($user->is_admin())
                        <li class="">
                            <a href="#" class="list-group-item-action">
                                <span class="sidebar-icon"><i class="la la-briefcase"></i> </span>
                                <span class="title">Jobs</span>
                                <span class="arrow"><i class="la la-arrow-right"></i> </span>
                            </a>

                            <ul class="dropdown-menu" style="display: none;">
                                <li><a class="sidebar-link" href="job-pending.html">Pending <span class="badge badge-success float-right">0</span></a> </li>
                                <li><a class="sidebar-link" href="job-approved.html">Approved  <span class="badge badge-success float-right">12</span> </a></li>
                                <li><a class="sidebar-link" href="job-blocked.html">Blocked  <span class="badge badge-success float-right">0</span> </a></li>
                            </ul>
                        </li>

                        <li class="">
                            <a href="flagged.html" class="list-group-item-action active">
                                <span class="sidebar-icon"><i class="la la-flag-o"></i> </span>
                                <span class="title">Flagged Jobs</span>
                            </a>
                        </li>


                        <li class="">
                            <a href="#" class="list-group-item-action">
                                <span class="sidebar-icon"><i class="la la-file-text-o"></i> </span>
                                <span class="title">CMS</span>
                                <span class="arrow"><i class="la la-arrow-right"></i> </span>
                            </a>

                            <ul class="dropdown-menu" style="display: none;">
                                <li><a class="sidebar-link" href="pages.html">Pages</a></li>
                                <li><a class="sidebar-link" href="posts.html">Posts</a></li>
                            </ul>
                        </li>


                        <li class="">
                            <a href="settings.html" class="list-group-item-action">
                                <span class="sidebar-icon"><i class="la la-cogs"></i> </span>
                                <span class="title">Settings</span>
                                <span class="arrow"><i class="la la-arrow-right"></i> </span>
                            </a>

                            <ul class="dropdown-menu" style="display: none;">
                                <li><a class="sidebar-link" href="settings.html">General Settings</a></li>
                                <li><a class="sidebar-link" href="pricing.html">Pricing</a></li>
                                <li><a class="sidebar-link" href="gateways.html">Gateways</a></li>
                            </ul>
                        </li>
                    @endif

                        <li class="">
                            <a href="{{route('payments')}}" class="list-group-item-action active">
                                <span class="sidebar-icon"><i class="la la-money"></i> </span>
                                <span class="title">Payments</span>
                            </a>
                        </li>


                    @if($user->is_admin())
                        <li class="">
                            <a href="users.html" class="list-group-item-action active">
                                <span class="sidebar-icon"><i class="la la-users"></i> </span>
                                <span class="title">Users</span>
                            </a>
                        </li>
                    @endif

                        <li class="">
                            <a href="{{route('profile')}}" class="list-group-item-action active">
                                <span class="sidebar-icon"><i class="la la-user"></i> </span>
                                <span class="title">Profile</span>
                            </a>
                        </li>


                        <li class="">
                            <a href="{{route('change_password')}}" class="list-group-item-action active">
                                <span class="sidebar-icon"><i class="la la-lock"></i> </span>
                                <span class="title">Change Password</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('logout') }}" class="list-group-item-action active"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <span class="sidebar-icon"><i class="la la-sign-out"></i> </span>
                                <span class="title">Logout</span>
                            </a>
                        </li>


                    </ul>
                </div>

            </div>
            @yield('content')
        </div>

    </div>
</div>

<!-- Scripts -->
<script src="{{asset('assets/admin/js/admin.js')}}" defer></script>

</body>
</html>