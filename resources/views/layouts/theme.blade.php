<!doctype html>
<html>
<head>
    <title>Job</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet" type="text/css" />
    <!--[if IE]>
    <script src="{{asset('assets/js/html5.js')}}"></script>
    <![endif]-->

</head>
<body>
<div class="mobile-nav"> <a href="/" class="logo-main"> <img src="{{asset('assets/images/logo.png')}}" alt="*" /></a>
    <nav>
        <ul class="unstyled mainnav pbpx-15">
            <li><a href="#">Jobs</a></li>
            <li><a href="#">Business <i class="xicon icon-angle-down"></i></a></li>
            <li><a href="#">International<i class="xicon icon-angle-down"></i></a></li>
            <li><a href="#">Wages</a></li>
            <li><a href="#">Tips</a></li>
        </ul>
    </nav>
</div>
<main class="app-container">
    <!-- Mobile Navigation Button Start-->
    <div class="mobile-nav-btn"> <span class="lines"></span> </div>
    <!-- Mobile Navigation Button End-->

    <header class="header-main">
        <div class="nav-area-full">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 logo-area">
                        <div class="logo"><a href="/"><img class="img-fluid" src="{{asset('assets/images/logo.png')}}" alt="*" /></a></div>
                    </div>
                    <div class="col-lg-6 d-flex">
                        <div class="main-menu align-self-center d-none d-lg-block">
                            <ul class="navbar-nav">
                                <li>
                                    <a href="{{route('jobs')}}">
                                        <i class="fa fa-building-o"></i> Jobs
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('business')}}">
                                        <i class="fa fa-handshake-o"></i> Business
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('international')}}">
                                        <i class="fa fa-globe"></i> International
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('wages')}}">
                                        <i class="fa fa-money"></i> Wages
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('tips')}}">
                                        <i class="fa fa-pencil-square-o"></i> Tips
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a href="javascript:;" class="nav-link btn btn-success text-white">
                                    <i class="la la-save"></i>Post new job
                                </a>
                            </li>
                            <!-- Authentication Links -->
                            @guest
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}"><i class="la la-sign-in"></i>{{ __('Login') }}</a>
                                </li>
                                @if (Route::has('register'))
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('new_register') }}"><i class="la la-user-plus"></i>{{ __('Register') }}</a>
                                    </li>
                                @endif
                            @else
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>
@yield('content')

    <div id="main-footer" class="main-footer bg-dark py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="footer-logo-wrap mb-3"><a href="javascript:;" class="navbar-brand"><img src="{{asset('assets/images/logo-alt.png')}}"></a></div>
                    <div class="footer-menu-wrap">
                        <ul class="list-unstyled">
                            <li><a href="javascript:;">Terms and Conditions </a></li>
                            <li><a href="javascript:;">Contact US</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="footer-menu-wrap mt-2">
                        <h4 class="mb-3">Worker</h4>
                        <ul class="list-unstyled">
                            <li><a href="javascript:;">Create Account</a></li>
                            <li><a href="javascript:;">Search Jobs</a></li>
                            <li><a href="javascript:;">Applied Jobs</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="footer-menu-wrap  mt-2">
                        <h4 class="mb-3">Employer</h4>
                        <ul class="list-unstyled">
                            <li><a href="javascript:;">Create Account</a></li>
                            <li><a href="javascript:;">Post new job</a></li>
                            <li><a href="javascript:;">Buy Premium Package</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="footer-copyright-text-wrap text-center mt-4">
                        <p>Copyright © 2020Sun Shine., all rights reserved</p>
                    </div>
                </div>
            </div>
        </div>
    </div>


</main>
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/slick.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/functions.js')}}"></script>
</body>
</html>