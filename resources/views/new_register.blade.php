@extends('layouts.theme')

@section('content')
    <div class="container">
        <div class="row justify-content-center" style="margin: 10% 0;">
            <div class="col-md-4">
                <div class="register-account-box jf-shadow p-3">
                    <h2>Worker</h2>
                    <p class="icon"><i class="fa fa-user"></i></p>
                    <p>Create an account to find and apply job and track your application</p>
                    <a href="{{route('worker')}}" class="btn btn-success">
                        <i class="fa fa-user-plus"></i> Register Account
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="register-account-box jf-shadow  p-3">
                    <h2>Employer</h2>
                    <p class="icon"><i class="fa fa-black-tie"></i></p>
                    <p>Create an employer account to post your job for find talented</p>
                    <a href="{{route('employer')}}" class="btn btn-success">
                        <i class="fa fa-user-plus"></i> Register Account
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection