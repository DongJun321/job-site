@extends('layouts.dashboard')
@section('content')
    <div class="col-md-9">
        <div class="main-page pr-4">

            <div class="main-page-title mt-3 mb-3 d-flex">
                <h3 class="flex-grow-1">Dashboard</h3>

                <div class="action-btn-group"></div>
            </div>
            @if(auth()->user()->is_admin())

                <div class="main-page-content p-4 mb-4">

                    <div class="row">
                        <div class="col-md-3">
                            <div class="p-3 mb-3 text-white bg-success">
                                <h4>Users</h4>
                                <h5>10</h5>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="p-3 mb-3 bg-warning">
                                <h4>Payments</h4>
                                <h5>&#36;230.00</h5>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="p-3 mb-3 bg-light">
                                <h4>Active Jobs</h4>
                                <h5>12</h5>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="p-3 mb-3 text-white bg-danger">
                                <h4>Total Jobs</h4>
                                <h5>12</h5>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="p-3 mb-3 text-white bg-dark">
                                <h4>Employer</h4>
                                <h5>3</h5>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="p-3 mb-3 text-white bg-info">
                                <h4>Agent</h4>
                                <h5>4</h5>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="p-3 mb-3 text-white bg-primary">
                                <h4>Applied</h4>
                                <h5>2</h5>
                            </div>
                        </div>
                    </div>
                    @else
                        @if(auth()->user()->is_employer())
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="no data-wrap py-5 my-5 text-center">
                                        {{--<h1 class="display-1"><i class="la la-frown-o"></i> </h1>--}}
                                        <h1>Employer Dashboard</h1>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if(auth()->user()->is_user())
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="no data-wrap py-5 my-5 text-center">
                                        {{--<h1 class="display-1"><i class="la la-frown-o"></i> </h1>--}}
                                        <h1>Worker Dashboard</h1>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endif

                </div>

                <div class="dashboard-footer mb-3">
                    <a href="#" target="_blank">Copyright © all rights reserved</a>
                </div>
        </div>

    </div>
@endsection