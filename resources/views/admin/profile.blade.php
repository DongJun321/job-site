@extends("layouts.dashboard")
@section('content')
    <div class="col-md-9">
        <div class="main-page pr-4">

            <div class="main-page-title mt-3 mb-3 d-flex">
                <h3 class="flex-grow-1">Profile</h3>

                <div class="action-btn-group"></div>
            </div>
            <div class="main-page-content p-4 mb-4">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered table-striped mb-4">
                            <tr>
                                <th>Name</th>
                                <td>{{auth()->user()->name}}</td>
                            </tr>
                            <tr>
                                <th>E-Mail</th>
                                <td>{{auth()->user()->email}}</td>
                            </tr>
                            <tr>
                                <th>Gender</th>
                                <td>{{auth()->user()->gender}}</td>
                            </tr>
                            <tr>
                                <th>Phone</th>
                                <td>{{auth()->user()->phone}}</td>
                            </tr>
                            <tr>
                                <th>Address</th>
                                <td>702 SW 8th St, Bentonville, AR 72716, USA</td>
                            </tr>
                            <tr>
                                <th>Country</th>
                                <td>
                                </td>
                            </tr>

                            <tr>
                                <th>Created At</th>
                                <td>{{auth()->user()->created_at}}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>Active</td>
                            </tr>
                        </table>
                        <a href="#"><i class="la la-pencil-square-o"></i> Edit </a>
                    </div>
                </div>
            </div>
            <div class="dashboard-footer mb-3">
                <a href="#" target="_blank">JobSite</a> Version 1.0.0
            </div>
        </div>
    </div>
@endsection