@extends("layouts.dashboard")
@section('content')
    <div class="col-md-9">
        <div class="main-page pr-4">

            <div class="main-page-title mt-3 mb-3 d-flex">
                <h3 class="flex-grow-1">Post new job</h3>

                <div class="action-btn-group"></div>
            </div>


            <div class="main-page-content p-4 mb-4">
                <div class="row">
                    <div class="col-md-10">

                        <form method="post" action="">
                            @csrf
                            <div class="form-group row ">
                                <label for="job_title" class="col-sm-4 control-label"> Job Title</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control " id="job_title" value="" name="job_title"
                                           placeholder="Job Title">
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="position" class="col-sm-4 control-label"> Position</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control " id="position" value="" name="position"
                                           placeholder="Position">
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="category" class="col-sm-4 control-label">Category</label>
                                <div class="col-sm-8">
                                    <select class="form-control " name="category" id="category">
                                        <option value="">Select Category</option>
                                        <option value="1">Accounting/Finance</option>
                                        <option value="20">Agro (Plant/Animal/Fisheries)</option>
                                        <option value="2">Bank/ Non-Bank Fin. Institution</option>
                                        <option value="13">Beauty Care/ Health &amp; Fitness</option>
                                        <option value="3">Commercial/Supply Chain</option>
                                        <option value="17">Customer Support/Call Centre</option>
                                        <option value="24">Data Entry/Operator/BPO</option>
                                        <option value="10">Design/Creative</option>
                                        <option value="25">Driving/Motor Technician</option>
                                        <option value="4">Education/Training</option>
                                        <option value="14">Electrician/ Construction/ Repair</option>
                                        <option value="5">Engineer/Architects</option>
                                        <option value="6">Garments/Textile</option>
                                        <option value="9">Gen Mgt/Admin</option>
                                        <option value="12">Hospitality/ Travel/ Tourism</option>
                                        <option value="7">HR/Org. Development</option>
                                        <option value="15">IT &amp; Telecommunication</option>
                                        <option value="27">Law/Legal</option>
                                        <option value="16">Marketing/Sales</option>
                                        <option value="18">Media/Ad./Event Mgt.</option>
                                        <option value="19">Medical/Pharma</option>
                                        <option value="21">NGO/Development</option>
                                        <option value="28">Others</option>
                                        <option value="11">Production/Operation</option>
                                        <option value="22">Research/Consultancy</option>
                                        <option value="23">Secretary/Receptionist</option>
                                        <option value="26">Security/Support Service</option>
                                    </select>


                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="salary_cycle" class="col-sm-4 control-label">Salary Cycle</label>
                                <div class="col-sm-8">

                                    <div class="price_input_group">

                                        <select class="form-control " name="salary_cycle">
                                            <option value="monthly">Monthly</option>
                                            <option value="yearly">Yearly</option>
                                            <option value="weekly">Weekly</option>
                                            <option value="daily">Daily</option>
                                            <option value="hourly">Hourly</option>

                                        </select>


                                    </div>
                                </div>
                            </div>


                            <div class="form-group row ">
                                <label for="salary" class="col-sm-4 control-label"> Salary</label>
                                <div class="col-sm-8">


                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <input type="number" class="form-control " id="salary" value=""
                                                   name="salary" placeholder="Salary">
                                        </div>
                                        <div class="col-md-6">
                                            <label> <input type="checkbox" name="is_negotiable" value="1"> Is Negotiable</label>
                                        </div>
                                    </div>


                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="salary_upto" class="col-sm-4 control-label"> Salary Upto</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control " id="salary_upto" value=""
                                           name="salary_upto" placeholder="Salary Upto">

                                    <p class="text-info">To show a salary range, place highest salary here</p>

                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="salary_currency" class="col-sm-4 control-label">Salary Currency</label>
                                <div class="col-sm-8">

                                    <div class="price_input_group">

                                        <select class="form-control " name="salary_currency">

                                            <option value="USD">USD | United States dollar</option>
                                            <option value="EUR">EUR | Euro</option>
                                            <option value="AED">AED | United Arab Emirates dirham</option>
                                            <option value="AFN">AFN | Afghan afghani</option>
                                            <option value="ALL">ALL | Albanian lek</option>
                                            <option value="AMD">AMD | Armenian dram</option>
                                            <option value="ANG">ANG | Netherlands Antillean guilder</option>
                                            <option value="AOA">AOA | Angolan kwanza</option>
                                            <option value="ARS">ARS | Argentine peso</option>
                                            <option value="AUD">AUD | Australian dollar</option>
                                            <option value="AWG">AWG | Aruban florin</option>
                                            <option value="AZN">AZN | Azerbaijani manat</option>
                                            <option value="BAM">BAM | Bosnia and Herzegovina convertible mark</option>
                                            <option value="BBD">BBD | Barbadian dollar</option>
                                            <option value="BDT">BDT | Bangladeshi taka</option>
                                            <option value="BGN">BGN | Bulgarian lev</option>
                                            <option value="BHD">BHD | Bahraini dinar</option>
                                            <option value="BIF">BIF | Burundian franc</option>
                                            <option value="BMD">BMD | Bermudian dollar</option>
                                            <option value="BND">BND | Brunei dollar</option>
                                            <option value="BOB">BOB | Bolivian boliviano</option>
                                            <option value="BRL">BRL | Brazilian real</option>
                                            <option value="BSD">BSD | Bahamian dollar</option>
                                            <option value="BTC">BTC | Bitcoin</option>
                                            <option value="BTN">BTN | Bhutanese ngultrum</option>
                                            <option value="BWP">BWP | Botswana pula</option>
                                            <option value="BYR">BYR | Belarusian ruble</option>
                                            <option value="BZD">BZD | Belize dollar</option>
                                            <option value="CAD">CAD | Canadian dollar</option>
                                            <option value="CDF">CDF | Congolese franc</option>
                                            <option value="CHF">CHF | Swiss franc</option>
                                            <option value="CLP">CLP | Chilean peso</option>
                                            <option value="CNY">CNY | Chinese yuan</option>
                                            <option value="COP">COP | Colombian peso</option>
                                            <option value="CRC">CRC | Costa Rican col&amp;oacute;n</option>
                                            <option value="CUC">CUC | Cuban convertible peso</option>
                                            <option value="CUP">CUP | Cuban peso</option>
                                            <option value="CVE">CVE | Cape Verdean escudo</option>
                                            <option value="CZK">CZK | Czech koruna</option>
                                            <option value="DJF">DJF | Djiboutian franc</option>
                                            <option value="DKK">DKK | Danish krone</option>
                                            <option value="DOP">DOP | Dominican peso</option>
                                            <option value="DZD">DZD | Algerian dinar</option>
                                            <option value="EGP">EGP | Egyptian pound</option>
                                            <option value="ERN">ERN | Eritrean nakfa</option>
                                            <option value="ETB">ETB | Ethiopian birr</option>
                                            <option value="FJD">FJD | Fijian dollar</option>
                                            <option value="FKP">FKP | Falkland Islands pound</option>
                                            <option value="GBP">GBP | Pound sterling</option>
                                            <option value="GEL">GEL | Georgian lari</option>
                                            <option value="GGP">GGP | Guernsey pound</option>
                                            <option value="GHS">GHS | Ghana cedi</option>
                                            <option value="GIP">GIP | Gibraltar pound</option>
                                            <option value="GMD">GMD | Gambian dalasi</option>
                                            <option value="GNF">GNF | Guinean franc</option>
                                            <option value="GTQ">GTQ | Guatemalan quetzal</option>
                                            <option value="GYD">GYD | Guyanese dollar</option>
                                            <option value="HKD">HKD | Hong Kong dollar</option>
                                            <option value="HNL">HNL | Honduran lempira</option>
                                            <option value="HRK">HRK | Croatian kuna</option>
                                            <option value="HTG">HTG | Haitian gourde</option>
                                            <option value="HUF">HUF | Hungarian forint</option>
                                            <option value="IDR">IDR | Indonesian rupiah</option>
                                            <option value="ILS">ILS | Israeli new shekel</option>
                                            <option value="IMP">IMP | Manx pound</option>
                                            <option value="INR">INR | Indian rupee</option>
                                            <option value="IQD">IQD | Iraqi dinar</option>
                                            <option value="IRR">IRR | Iranian rial</option>
                                            <option value="ISK">ISK | Icelandic kr&amp;oacute;na</option>
                                            <option value="JEP">JEP | Jersey pound</option>
                                            <option value="JMD">JMD | Jamaican dollar</option>
                                            <option value="JOD">JOD | Jordanian dinar</option>
                                            <option value="JPY">JPY | Japanese yen</option>
                                            <option value="KES">KES | Kenyan shilling</option>
                                            <option value="KGS">KGS | Kyrgyzstani som</option>
                                            <option value="KHR">KHR | Cambodian riel</option>
                                            <option value="KMF">KMF | Comorian franc</option>
                                            <option value="KPW">KPW | North Korean won</option>
                                            <option value="KRW">KRW | South Korean won</option>
                                            <option value="KWD">KWD | Kuwaiti dinar</option>
                                            <option value="KYD">KYD | Cayman Islands dollar</option>
                                            <option value="KZT">KZT | Kazakhstani tenge</option>
                                            <option value="LAK">LAK | Lao kip</option>
                                            <option value="LBP">LBP | Lebanese pound</option>
                                            <option value="LKR">LKR | Sri Lankan rupee</option>
                                            <option value="LRD">LRD | Liberian dollar</option>
                                            <option value="LSL">LSL | Lesotho loti</option>
                                            <option value="LYD">LYD | Libyan dinar</option>
                                            <option value="MAD">MAD | Moroccan dirham</option>
                                            <option value="MDL">MDL | Moldovan leu</option>
                                            <option value="MGA">MGA | Malagasy ariary</option>
                                            <option value="MKD">MKD | Macedonian denar</option>
                                            <option value="MMK">MMK | Burmese kyat</option>
                                            <option value="MNT">MNT | Mongolian t&amp;ouml;gr&amp;ouml;g</option>
                                            <option value="MOP">MOP | Macanese pataca</option>
                                            <option value="MRO">MRO | Mauritanian ouguiya</option>
                                            <option value="MUR">MUR | Mauritian rupee</option>
                                            <option value="MVR">MVR | Maldivian rufiyaa</option>
                                            <option value="MWK">MWK | Malawian kwacha</option>
                                            <option value="MXN">MXN | Mexican peso</option>
                                            <option value="MYR">MYR | Malaysian ringgit</option>
                                            <option value="MZN">MZN | Mozambican metical</option>
                                            <option value="NAD">NAD | Namibian dollar</option>
                                            <option value="NGN">NGN | Nigerian naira</option>
                                            <option value="NIO">NIO | Nicaraguan c&amp;oacute;rdoba</option>
                                            <option value="NOK">NOK | Norwegian krone</option>
                                            <option value="NPR">NPR | Nepalese rupee</option>
                                            <option value="NZD">NZD | New Zealand dollar</option>
                                            <option value="OMR">OMR | Omani rial</option>
                                            <option value="PAB">PAB | Panamanian balboa</option>
                                            <option value="PEN">PEN | Peruvian nuevo sol</option>
                                            <option value="PGK">PGK | Papua New Guinean kina</option>
                                            <option value="PHP">PHP | Philippine peso</option>
                                            <option value="PKR">PKR | Pakistani rupee</option>
                                            <option value="PLN">PLN | Polish z&amp;#x142;oty</option>
                                            <option value="PRB">PRB | Transnistrian ruble</option>
                                            <option value="PYG">PYG | Paraguayan guaran&amp;iacute;</option>
                                            <option value="QAR">QAR | Qatari riyal</option>
                                            <option value="RON">RON | Romanian leu</option>
                                            <option value="RSD">RSD | Serbian dinar</option>
                                            <option value="RUB">RUB | Russian ruble</option>
                                            <option value="RWF">RWF | Rwandan franc</option>
                                            <option value="SAR">SAR | Saudi riyal</option>
                                            <option value="SBD">SBD | Solomon Islands dollar</option>
                                            <option value="SCR">SCR | Seychellois rupee</option>
                                            <option value="SDG">SDG | Sudanese pound</option>
                                            <option value="SEK">SEK | Swedish krona</option>
                                            <option value="SGD">SGD | Singapore dollar</option>
                                            <option value="SHP">SHP | Saint Helena pound</option>
                                            <option value="SLL">SLL | Sierra Leonean leone</option>
                                            <option value="SOS">SOS | Somali shilling</option>
                                            <option value="SRD">SRD | Surinamese dollar</option>
                                            <option value="SSP">SSP | South Sudanese pound</option>
                                            <option value="STD">STD | S&amp;atilde;o Tom&amp;eacute; and Pr&amp;iacute;ncipe
                                                dobra
                                            </option>
                                            <option value="SYP">SYP | Syrian pound</option>
                                            <option value="SZL">SZL | Swazi lilangeni</option>
                                            <option value="THB">THB | Thai baht</option>
                                            <option value="TJS">TJS | Tajikistani somoni</option>
                                            <option value="TMT">TMT | Turkmenistan manat</option>
                                            <option value="TND">TND | Tunisian dinar</option>
                                            <option value="TOP">TOP | Tongan pa&amp;#x2bb;anga</option>
                                            <option value="TRY">TRY | Turkish lira</option>
                                            <option value="TTD">TTD | Trinidad and Tobago dollar</option>
                                            <option value="TWD">TWD | New Taiwan dollar</option>
                                            <option value="TZS">TZS | Tanzanian shilling</option>
                                            <option value="UAH">UAH | Ukrainian hryvnia</option>
                                            <option value="UGX">UGX | Ugandan shilling</option>
                                            <option value="UYU">UYU | Uruguayan peso</option>
                                            <option value="UZS">UZS | Uzbekistani som</option>
                                            <option value="VEF">VEF | Venezuelan bol&amp;iacute;var</option>
                                            <option value="VND">VND | Vietnamese &amp;#x111;&amp;#x1ed3;ng</option>
                                            <option value="VUV">VUV | Vanuatu vatu</option>
                                            <option value="WST">WST | Samoan t&amp;#x101;l&amp;#x101;</option>
                                            <option value="XAF">XAF | Central African CFA franc</option>
                                            <option value="XCD">XCD | East Caribbean dollar</option>
                                            <option value="XOF">XOF | West African CFA franc</option>
                                            <option value="XPF">XPF | CFP franc</option>
                                            <option value="YER">YER | Yemeni rial</option>
                                            <option value="ZAR">ZAR | South African rand</option>
                                            <option value="ZMW">ZMW | Zambian kwacha</option>
                                        </select>


                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="vacancy" class="col-sm-4 control-label"> Vacancy</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control " id="vacancy" value="" name="vacancy"
                                           placeholder="Vacancy">


                                </div>
                            </div>


                            <div class="form-group row ">
                                <label for="gender" class="col-sm-4 control-label">Gender</label>
                                <div class="col-sm-8">
                                    <select class="form-control " name="gender" id="gender">
                                        <option value="any">Any</option>
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                        <option value="transgender">Transgender</option>
                                    </select>


                                </div>
                            </div>


                            <div class="form-group row ">
                                <label for="exp_level" class="col-sm-4 control-label">Experience Level</label>
                                <div class="col-sm-8">
                                    <select class="form-control " name="exp_level" id="exp_level">
                                        <option value="mid">Medium</option>
                                        <option value="entry">Entry</option>
                                        <option value="senior">Senior</option>
                                    </select>


                                </div>
                            </div>


                            <div class="form-group row ">
                                <label for="job_type" class="col-sm-4 control-label">Job Type</label>
                                <div class="col-sm-8">
                                    <select class="form-control " name="job_type" id="job_type">
                                        <option value="full_time">Full Time</option>
                                        <option value="internship">Internship</option>
                                        <option value="part_time">Part-time</option>
                                        <option value="contract">Contract</option>
                                        <option value="temporary">Temporary</option>
                                        <option value="commission">Commission</option>
                                        <option value="internship">Internship</option>
                                    </select>


                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="experience_required_years" class="col-sm-4 control-label"> Experience
                                    Required (in year/s)</label>
                                <div class="col-sm-8">

                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <input type="number" class="form-control " id="experience_required_years"
                                                   value="" name="experience_required_years"
                                                   placeholder="Experience Required (in year/s)">
                                        </div>
                                        <div class="col-md-6">
                                            <label> <input type="checkbox" name="experience_plus" value="1">
                                                Plus</label>
                                        </div>
                                    </div>


                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="deadline" class="col-sm-4 control-label"> Deadline</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control  date_picker" id="deadline" value=""
                                           name="deadline" placeholder="Deadline">


                                </div>
                            </div>

                            <div class="form-group row ">
                                <label class="col-sm-4 control-label"> Description</label>
                                <div class="col-sm-8">
                                    <textarea name="description" class="form-control " rows="5"></textarea>

                                    <p class="text-info"> Write your job description Elaborately</p>
                                </div>
                            </div>


                            <div class="form-group row ">
                                <label class="col-sm-4 control-label"> Skills</label>
                                <div class="col-sm-8">
                                    <textarea name="skills" class="form-control " rows="2"></textarea>

                                    <p class="text-info"> Place required skills, separate by comma(,) </p>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label class="col-sm-4 control-label"> Responsibilities</label>
                                <div class="col-sm-8">
                                    <textarea name="responsibilities" class="form-control " rows="3"></textarea>

                                    <p class="text-info"> Write one line per responsibilities</p>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label class="col-sm-4 control-label"> Educational Requirements</label>
                                <div class="col-sm-8">
                                    <textarea name="educational_requirements" class="form-control " rows="3"></textarea>

                                    <p class="text-info"> Write one line per educational requirements</p>
                                </div>
                            </div>


                            <div class="form-group row ">
                                <label class="col-sm-4 control-label"> Experience Requirements</label>
                                <div class="col-sm-8">
                                    <textarea name="experience_requirements" class="form-control " rows="3"></textarea>

                                    <p class="text-info"> Write one line per experience requirements</p>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label class="col-sm-4 control-label"> Additional Requirements</label>
                                <div class="col-sm-8">
                                    <textarea name="additional_requirements" class="form-control " rows="3"></textarea>

                                    <p class="text-info"> Write one line per additional requirements</p>
                                </div>
                            </div>


                            <div class="form-group row ">
                                <label class="col-sm-4 control-label"> Benefits</label>
                                <div class="col-sm-8">
                                    <textarea name="benefits" class="form-control " rows="3"></textarea>

                                    <p class="text-info"> Company offering which benefits? Write one line per
                                        benefits</p>
                                </div>
                            </div>


                            <div class="form-group row ">
                                <label class="col-sm-4 control-label"> Apply instruction</label>
                                <div class="col-sm-8">
                                    <textarea name="apply_instruction" class="form-control " rows="3"></textarea>

                                    <p class="text-info"> Tell here to applicants how they should apply</p>
                                </div>
                            </div>

                            <legend>Job Location</legend>


                            <div class="form-group row ">
                                <label for="is_any_where" class="col-md-4 control-label">Is any where </label>
                                <div class="col-md-8">
                                    <label> <input type="checkbox" name="is_any_where" value="1"> Location Anywhere
                                    </label>

                                </div>
                            </div>


                            <div class="form-group row ">
                                <label for="country" class="col-md-4 control-label">Country <span
                                            class="mendatory-mark">*</span></label>
                                <div class="col-md-8">
                                    <select name="country" class="form-control  country_to_state">
                                        <option value="">Select a country</option>
                                        <option value="1">Afghanistan</option>
                                        <option value="2">Albania</option>
                                        <option value="3">Algeria</option>
                                        <option value="4">American Samoa</option>
                                        <option value="5">Andorra</option>
                                        <option value="6">Angola</option>
                                        <option value="7">Anguilla</option>
                                        <option value="8">Antarctica</option>
                                        <option value="9">Antigua And Barbuda</option>
                                        <option value="10">Argentina</option>
                                        <option value="11">Armenia</option>
                                        <option value="12">Aruba</option>
                                        <option value="13">Australia</option>
                                        <option value="14">Austria</option>
                                        <option value="15">Azerbaijan</option>
                                        <option value="16">Bahamas The</option>
                                        <option value="17">Bahrain</option>
                                        <option value="18">Bangladesh</option>
                                        <option value="19">Barbados</option>
                                        <option value="20">Belarus</option>
                                        <option value="21">Belgium</option>
                                        <option value="22">Belize</option>
                                        <option value="23">Benin</option>
                                        <option value="24">Bermuda</option>
                                        <option value="25">Bhutan</option>
                                        <option value="26">Bolivia</option>
                                        <option value="27">Bosnia and Herzegovina</option>
                                        <option value="28">Botswana</option>
                                        <option value="29">Bouvet Island</option>
                                        <option value="30">Brazil</option>
                                        <option value="31">British Indian Ocean Territory</option>
                                        <option value="32">Brunei</option>
                                        <option value="33">Bulgaria</option>
                                        <option value="34">Burkina Faso</option>
                                        <option value="35">Burundi</option>
                                        <option value="36">Cambodia</option>
                                        <option value="37">Cameroon</option>
                                        <option value="38">Canada</option>
                                        <option value="39">Cape Verde</option>
                                        <option value="40">Cayman Islands</option>
                                        <option value="41">Central African Republic</option>
                                        <option value="42">Chad</option>
                                        <option value="43">Chile</option>
                                        <option value="44">China</option>
                                        <option value="45">Christmas Island</option>
                                        <option value="46">Cocos (Keeling) Islands</option>
                                        <option value="47">Colombia</option>
                                        <option value="48">Comoros</option>
                                        <option value="49">Congo</option>
                                        <option value="50">Congo The Democratic Republic Of The</option>
                                        <option value="51">Cook Islands</option>
                                        <option value="52">Costa Rica</option>
                                        <option value="53">Cote D'Ivoire (Ivory Coast)</option>
                                        <option value="54">Croatia (Hrvatska)</option>
                                        <option value="55">Cuba</option>
                                        <option value="56">Cyprus</option>
                                        <option value="57">Czech Republic</option>
                                        <option value="58">Denmark</option>
                                        <option value="59">Djibouti</option>
                                        <option value="60">Dominica</option>
                                        <option value="61">Dominican Republic</option>
                                        <option value="62">East Timor</option>
                                        <option value="63">Ecuador</option>
                                        <option value="64">Egypt</option>
                                        <option value="65">El Salvador</option>
                                        <option value="66">Equatorial Guinea</option>
                                        <option value="67">Eritrea</option>
                                        <option value="68">Estonia</option>
                                        <option value="69">Ethiopia</option>
                                        <option value="70">External Territories of Australia</option>
                                        <option value="71">Falkland Islands</option>
                                        <option value="72">Faroe Islands</option>
                                        <option value="73">Fiji Islands</option>
                                        <option value="74">Finland</option>
                                        <option value="75">France</option>
                                        <option value="76">French Guiana</option>
                                        <option value="77">French Polynesia</option>
                                        <option value="78">French Southern Territories</option>
                                        <option value="79">Gabon</option>
                                        <option value="80">Gambia The</option>
                                        <option value="81">Georgia</option>
                                        <option value="82">Germany</option>
                                        <option value="83">Ghana</option>
                                        <option value="84">Gibraltar</option>
                                        <option value="85">Greece</option>
                                        <option value="86">Greenland</option>
                                        <option value="87">Grenada</option>
                                        <option value="88">Guadeloupe</option>
                                        <option value="89">Guam</option>
                                        <option value="90">Guatemala</option>
                                        <option value="91">Guernsey and Alderney</option>
                                        <option value="92">Guinea</option>
                                        <option value="93">Guinea-Bissau</option>
                                        <option value="94">Guyana</option>
                                        <option value="95">Haiti</option>
                                        <option value="96">Heard and McDonald Islands</option>
                                        <option value="97">Honduras</option>
                                        <option value="98">Hong Kong S.A.R.</option>
                                        <option value="99">Hungary</option>
                                        <option value="100">Iceland</option>
                                        <option value="101">India</option>
                                        <option value="102">Indonesia</option>
                                        <option value="103">Iran</option>
                                        <option value="104">Iraq</option>
                                        <option value="105">Ireland</option>
                                        <option value="106">Israel</option>
                                        <option value="107">Italy</option>
                                        <option value="108">Jamaica</option>
                                        <option value="109">Japan</option>
                                        <option value="110">Jersey</option>
                                        <option value="111">Jordan</option>
                                        <option value="112">Kazakhstan</option>
                                        <option value="113">Kenya</option>
                                        <option value="114">Kiribati</option>
                                        <option value="115">Korea North</option>
                                        <option value="116">Korea South</option>
                                        <option value="117">Kuwait</option>
                                        <option value="118">Kyrgyzstan</option>
                                        <option value="119">Laos</option>
                                        <option value="120">Latvia</option>
                                        <option value="121">Lebanon</option>
                                        <option value="122">Lesotho</option>
                                        <option value="123">Liberia</option>
                                        <option value="124">Libya</option>
                                        <option value="125">Liechtenstein</option>
                                        <option value="126">Lithuania</option>
                                        <option value="127">Luxembourg</option>
                                        <option value="128">Macau S.A.R.</option>
                                        <option value="129">Macedonia</option>
                                        <option value="130">Madagascar</option>
                                        <option value="131">Malawi</option>
                                        <option value="132">Malaysia</option>
                                        <option value="133">Maldives</option>
                                        <option value="134">Mali</option>
                                        <option value="135">Malta</option>
                                        <option value="136">Man (Isle of)</option>
                                        <option value="137">Marshall Islands</option>
                                        <option value="138">Martinique</option>
                                        <option value="139">Mauritania</option>
                                        <option value="140">Mauritius</option>
                                        <option value="141">Mayotte</option>
                                        <option value="142">Mexico</option>
                                        <option value="143">Micronesia</option>
                                        <option value="144">Moldova</option>
                                        <option value="145">Monaco</option>
                                        <option value="146">Mongolia</option>
                                        <option value="147">Montserrat</option>
                                        <option value="148">Morocco</option>
                                        <option value="149">Mozambique</option>
                                        <option value="150">Myanmar</option>
                                        <option value="151">Namibia</option>
                                        <option value="152">Nauru</option>
                                        <option value="153">Nepal</option>
                                        <option value="154">Netherlands Antilles</option>
                                        <option value="155">Netherlands The</option>
                                        <option value="156">New Caledonia</option>
                                        <option value="157">New Zealand</option>
                                        <option value="158">Nicaragua</option>
                                        <option value="159">Niger</option>
                                        <option value="160">Nigeria</option>
                                        <option value="161">Niue</option>
                                        <option value="162">Norfolk Island</option>
                                        <option value="163">Northern Mariana Islands</option>
                                        <option value="164">Norway</option>
                                        <option value="165">Oman</option>
                                        <option value="166">Pakistan</option>
                                        <option value="167">Palau</option>
                                        <option value="168">Palestinian Territory Occupied</option>
                                        <option value="169">Panama</option>
                                        <option value="170">Papua new Guinea</option>
                                        <option value="171">Paraguay</option>
                                        <option value="172">Peru</option>
                                        <option value="173">Philippines</option>
                                        <option value="174">Pitcairn Island</option>
                                        <option value="175">Poland</option>
                                        <option value="176">Portugal</option>
                                        <option value="177">Puerto Rico</option>
                                        <option value="178">Qatar</option>
                                        <option value="179">Reunion</option>
                                        <option value="180">Romania</option>
                                        <option value="181">Russia</option>
                                        <option value="182">Rwanda</option>
                                        <option value="183">Saint Helena</option>
                                        <option value="184">Saint Kitts And Nevis</option>
                                        <option value="185">Saint Lucia</option>
                                        <option value="186">Saint Pierre and Miquelon</option>
                                        <option value="187">Saint Vincent And The Grenadines</option>
                                        <option value="188">Samoa</option>
                                        <option value="189">San Marino</option>
                                        <option value="190">Sao Tome and Principe</option>
                                        <option value="191">Saudi Arabia</option>
                                        <option value="192">Senegal</option>
                                        <option value="193">Serbia</option>
                                        <option value="194">Seychelles</option>
                                        <option value="195">Sierra Leone</option>
                                        <option value="196">Singapore</option>
                                        <option value="197">Slovakia</option>
                                        <option value="198">Slovenia</option>
                                        <option value="199">Smaller Territories of the UK</option>
                                        <option value="200">Solomon Islands</option>
                                        <option value="201">Somalia</option>
                                        <option value="202">South Africa</option>
                                        <option value="203">South Georgia</option>
                                        <option value="204">South Sudan</option>
                                        <option value="205">Spain</option>
                                        <option value="206">Sri Lanka</option>
                                        <option value="207">Sudan</option>
                                        <option value="208">Suriname</option>
                                        <option value="209">Svalbard And Jan Mayen Islands</option>
                                        <option value="210">Swaziland</option>
                                        <option value="211">Sweden</option>
                                        <option value="212">Switzerland</option>
                                        <option value="213">Syria</option>
                                        <option value="214">Taiwan</option>
                                        <option value="215">Tajikistan</option>
                                        <option value="216">Tanzania</option>
                                        <option value="217">Thailand</option>
                                        <option value="218">Togo</option>
                                        <option value="219">Tokelau</option>
                                        <option value="220">Tonga</option>
                                        <option value="221">Trinidad And Tobago</option>
                                        <option value="222">Tunisia</option>
                                        <option value="223">Turkey</option>
                                        <option value="224">Turkmenistan</option>
                                        <option value="225">Turks And Caicos Islands</option>
                                        <option value="226">Tuvalu</option>
                                        <option value="227">Uganda</option>
                                        <option value="228">Ukraine</option>
                                        <option value="229">United Arab Emirates</option>
                                        <option value="230">United Kingdom</option>
                                        <option value="231">United States</option>
                                        <option value="232">United States Minor Outlying Islands</option>
                                        <option value="233">Uruguay</option>
                                        <option value="234">Uzbekistan</option>
                                        <option value="235">Vanuatu</option>
                                        <option value="236">Vatican City State (Holy See)</option>
                                        <option value="237">Venezuela</option>
                                        <option value="238">Vietnam</option>
                                        <option value="239">Virgin Islands (British)</option>
                                        <option value="240">Virgin Islands (US)</option>
                                        <option value="241">Wallis And Futuna Islands</option>
                                        <option value="242">Western Sahara</option>
                                        <option value="243">Yemen</option>
                                        <option value="244">Yugoslavia</option>
                                        <option value="245">Zambia</option>
                                        <option value="246">Zimbabwe</option>
                                    </select>


                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="state" class="col-md-4 control-label">State </label>
                                <div class="col-md-8">
                                    <select name="state" class="form-control  state_options">
                                        <option value="">Select a state</option>


                                    </select>

                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="city_name" class="col-sm-4 control-label"> City Name</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control " id="city_name" value="" name="city_name"
                                           placeholder="City Name">


                                </div>
                            </div>


                            <div class="alert alert-warning">

                                <legend>Premium Job</legend>

                                <div class="form-group row ">
                                    <label for="is_premium" class="col-md-4 control-label">Is Premium </label>
                                    <div class="col-md-8">

                                        <label> <input type="checkbox" name="is_premium" value="1"> Location Anywhere
                                        </label>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4"></label>
                                <div class="col-sm-8">
                                    <button type="submit" class="btn btn-primary">Post new job</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="dashboard-footer mb-3">
                <a href="#" target="_blank">JobSite</a>
                Version 1.0.0
            </div>
        </div>

    </div>
@endsection