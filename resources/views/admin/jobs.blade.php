@extends('layouts.dashboard')
@section('content')
    <div class="col-md-9">
        <div class="main-page pr-4">
            <div class="main-page-title mt-3 mb-3 d-flex">
                <h3 class="flex-grow-1">Posted Jobs</h3>
                <div class="action-btn-group"></div>
            </div>
            <div class="main-page-content p-4 mb-4">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered">
                            <tr>
                                <th>Job Title</th>
                                <th>Status</th>
                                <th>Employer</th>
                                <th>#</th>
                            </tr>
                            @foreach($jobs as $job)
                            <tr>
                                <td>{{$job->job_title}}
                                    <p class="text-muted">Deadline {{$job->deadline}}</p>
                                    <p class="text-muted"> <a href="">Applicant (0) </a>  </p>
                                </td>
                                <td>
                                    <span class="text-success">{{$job->status}}</span>
                                </td>
                                <td>{{$job->description}}</td>
                                <td>
                                    <a href="" class="btn btn-primary btn-sm" target="_blank" data-toggle="tooltip" title="View"><i class="la la-eye"></i> </a>
                                    <a href="" class="btn btn-secondary btn-sm"><i class="la la-edit" data-toggle="tooltip" title="Edit"></i> </a>
                                    <a href="" class="btn btn-success btn-sm" data-toggle="tooltip" title="Mark As Premium"><i class="la la-bookmark-o"></i> </a>
                                    <a href="" class="btn btn-warning btn-sm" data-toggle="tooltip" title="Block"><i class="la la-ban"></i> </a>
                                    <a href="" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="la la-trash-o"></i> </a>
                                </td>
                            </tr>
                            @endforeach
                        </table>


                    </div>
                </div>
            </div>
            <div class="dashboard-footer mb-3">
                <a href="" target="_blank">JobSite</a> Version 1.0.0
            </div>
        </div>
    </div>
@endsection