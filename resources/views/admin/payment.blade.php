@extends('layouts.dashboard')
@section('content')
    <div class="col-md-9">
        <div class="main-page pr-4">

            <div class="main-page-title mt-3 mb-3 d-flex">
                <h3 class="flex-grow-1">Payments</h3>

                <div class="action-btn-group"></div>
            </div>


            <div class="main-page-content p-4 mb-4">

                <div class="row mb-4">
                    <div class="col-md-5">
                        Total : 9
                    </div>

                    <div class="col-md-7">
                        <form class="form-inline" method="get" action="">
                            <div class="form-group">
                                <input type="text" name="q" value="" class="form-control" placeholder="Payer email">
                            </div>
                            <button type="submit" class="btn btn-secondary">Search</button>
                        </form>

                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">

                        <table class="table table-striped table-bordered">

                            <tr>
                                <th>Name</th>
                                <th>Payer email</th>
                                <th>Amount</th>
                                <th>Method</th>
                                <th>Time</th>
                                <th>#</th>
                            </tr>

                            <tr>
                                <td>
                                    <a href="#">
                                        <i class="la la-user"></i> andres arribs <br />
                                        <i class="la la-building-o"></i> casafilo
                                    </a>
                                </td>
                                <td><a href="#"> 1restaurantecasafilo@gmail.com </a></td>
                                <td>&#36;30.00</td>
                                <td>stripe</td>
                                <td><span data-toggle="tooltip" title="April 04, 2020 07:41 pm">April 04, 2020</span></td>

                                <td>
                                    <span class="text-danger" data-toggle="tooltip" title="initial"><i class="la la-exclamation-circle"></i> </span>

                                    <a href="#" class="btn btn-success ml-2"><i class="la la-eye"></i> </a>
                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <a href="#">
                                        <i class="la la-user"></i> andres arribs <br />
                                        <i class="la la-building-o"></i> casafilo
                                    </a>
                                </td>
                                <td><a href="#"> 1restaurantecasafilo@gmail.com </a></td>
                                <td>&#36;30.00</td>
                                <td>paypal</td>
                                <td><span data-toggle="tooltip" title="April 04, 2020 07:41 pm">April 04, 2020</span></td>

                                <td>
                                    <span class="text-danger" data-toggle="tooltip" title="initial"><i class="la la-exclamation-circle"></i> </span>

                                    <a href="#" class="btn btn-success ml-2"><i class="la la-eye"></i> </a>
                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <a href="#">
                                        <i class="la la-user"></i> Stephen King <br />
                                        <i class="la la-building-o"></i> Google
                                    </a>
                                </td>
                                <td><a href="#"> stephen@demo.com </a></td>
                                <td>&#36;30.00</td>
                                <td>stripe</td>
                                <td><span data-toggle="tooltip" title="December 09, 2018 02:03 pm">December 09, 2018</span></td>

                                <td>
                                    <span class="text-success" data-toggle="tooltip" title="success"><i class="la la-check-circle-o"></i> </span>

                                    <a href="#" class="btn btn-success ml-2"><i class="la la-eye"></i> </a>
                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <a href="https://hiljob.com/dashboard/payments/view/6">
                                        <i class="la la-user"></i> Eric C. Hyde <br />
                                        <i class="la la-building-o"></i> TriangleDream
                                    </a>
                                </td>
                                <td><a href="#"> eric@demo.com </a></td>
                                <td>&#36;30.00</td>
                                <td>stripe</td>
                                <td><span data-toggle="tooltip" title="December 09, 2018 01:04 pm">December 09, 2018</span></td>

                                <td>
                                    <span class="text-success" data-toggle="tooltip" title="success"><i class="la la-check-circle-o"></i> </span>

                                    <a href="" class="btn btn-success ml-2"><i class="la la-eye"></i> </a>
                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <a href="#">
                                        <i class="la la-user"></i> John Doe <br />
                                        <i class="la la-building-o"></i> Walmart
                                    </a>
                                </td>
                                <td><a href="#"> admin@demo.com </a></td>
                                <td>&#36;30.00</td>
                                <td>bank_transfer</td>
                                <td><span data-toggle="tooltip" title="December 08, 2018 05:00 pm">December 08, 2018</span></td>

                                <td>
                                    <span class="text-success" data-toggle="tooltip" title="success"><i class="la la-check-circle-o"></i> </span>

                                    <a href="#" class="btn btn-success ml-2"><i class="la la-eye"></i> </a>
                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <a href="#">
                                        <i class="la la-user"></i> John Doe <br />
                                        <i class="la la-building-o"></i> Walmart
                                    </a>
                                </td>
                                <td><a href="#"> admin@demo.com </a></td>
                                <td>&#36;30.00</td>
                                <td>stripe</td>
                                <td><span data-toggle="tooltip" title="December 08, 2018 04:59 pm">December 08, 2018</span></td>

                                <td>
                                    <span class="text-danger" data-toggle="tooltip" title="initial"><i class="la la-exclamation-circle"></i> </span>

                                    <a href="#" class="btn btn-success ml-2"><i class="la la-eye"></i> </a>
                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <a href="#">
                                        <i class="la la-user"></i> John Doe <br />
                                        <i class="la la-building-o"></i> Walmart
                                    </a>
                                </td>
                                <td><a href="#"> admin@demo.com </a></td>
                                <td>&#36;30.00</td>
                                <td>stripe</td>
                                <td><span data-toggle="tooltip" title="December 08, 2018 04:33 pm">December 08, 2018</span></td>

                                <td>
                                    <span class="text-success" data-toggle="tooltip" title="success"><i class="la la-check-circle-o"></i> </span>

                                    <a href="#" class="btn btn-success ml-2"><i class="la la-eye"></i> </a>
                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <a href="#">
                                        <i class="la la-user"></i> John Doe <br />
                                        <i class="la la-building-o"></i> Walmart
                                    </a>
                                </td>
                                <td><a href="#"> admin@demo.com </a></td>
                                <td>&#36;80.00</td>
                                <td>stripe</td>
                                <td><span data-toggle="tooltip" title="December 08, 2018 03:42 pm">December 08, 2018</span></td>

                                <td>
                                    <span class="text-success" data-toggle="tooltip" title="success"><i class="la la-check-circle-o"></i> </span>

                                    <a href="#" class="btn btn-success ml-2"><i class="la la-eye"></i> </a>
                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <a href="#">
                                        <i class="la la-user"></i> John Doe <br />
                                        <i class="la la-building-o"></i> Walmart
                                    </a>
                                </td>
                                <td><a href="#"> admin@demo.com </a></td>
                                <td>&#36;30.00</td>
                                <td>paypal</td>
                                <td><span data-toggle="tooltip" title="December 08, 2018 01:34 pm">December 08, 2018</span></td>

                                <td>
                                    <span class="text-success" data-toggle="tooltip" title="success"><i class="la la-check-circle-o"></i> </span>

                                    <a href="#" class="btn btn-success ml-2"><i class="la la-eye"></i> </a>
                                </td>

                            </tr>

                        </table>
                    </div>
                </div>
            </div>
            <div class="dashboard-footer mb-3">
                <a href="#" target="_blank">JobSite</a> Version 1.0.0
            </div>
        </div>
    </div>
@endsection