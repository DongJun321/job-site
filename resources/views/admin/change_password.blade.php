@extends("layouts.dashboard")
@section('content')
    <div class="col-md-9">
        <div class="main-page pr-4">

            <div class="main-page-title mt-3 mb-3 d-flex">
                <h3 class="flex-grow-1">Change Password</h3>
                <div class="action-btn-group"></div>
            </div>
            <div class="main-page-content p-4 mb-4">
                <div class="row">
                    <div class="col-md-12">
                        <form action="" method="post">
                            <input type="hidden" name="_token" value="ZHEnMtE0GjmE6nUGO4EmOqrV0weUBfqGqU8RHMPK">
                            <div class="form-group row ">
                                <label class="col-sm-3 control-label" for="old_password">Old password *</label>
                                <div class="col-sm-9">
                                    <input type="password" name="old_password" id="old_password" class="form-control" value="" autocomplete="off" placeholder="Old password " />

                                </div>
                            </div>

                            <div class="form-group row ">
                                <label class="col-sm-3 control-label" for="new_password">New password *</label>
                                <div class="col-sm-9">
                                    <input type="password" name="new_password" id="new_password" class="form-control" value="" autocomplete="off" placeholder="New password" />

                                </div>
                            </div>
                            <div class="form-group row ">
                                <label class="col-sm-3 control-label" for="new_password_confirmation">Old password confirmation *</label>
                                <div class="col-sm-9">
                                    <input type="password" name="new_password_confirmation" id="new_password_confirmation" class="form-control" value="" autocomplete="off" placeholder="Old password confirmation" />

                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="offset-md-3 col-md-9">
                                    <button type="submit" class="btn btn-success">Change Password</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="dashboard-footer mb-3">
                <a href="https://www.themeqx.com/product/jobfair-job-board-application" target="_blank">JobFair</a> Version 1.0.0
            </div>
        </div>

    </div>
@endsection