@extends('layouts.dashboard')
@section('content')
    <div class="col-md-9">
        <div class="main-page pr-4">

            <div class="main-page-title mt-3 mb-3 d-flex">
                <h3 class="flex-grow-1">Applicant</h3>

                <div class="action-btn-group"></div>
            </div>


            <div class="main-page-content p-4 mb-4">
                <div class="row">
                    <div class="col-md-12">

                        <table class="table table-bordered">

                            <tr>
                                <th>Name</th>
                                <th>Employer</th>
                            </tr>

                            <tr>
                                <td>
                                    <i class="la la-user"></i> Jhony Deep
                                    <p class="text-muted"><i class="la la-clock-o"></i> November 27, 2018 1:27 PM</p>
                                    <p class="text-muted"><i class="la la-envelope-o"></i> jhony@demo.com</p>
                                    <p class="text-muted"><i class="la la-phone-square"></i> 112234354</p>
                                </td>

                                <td>
                                    <p>
                                        <a href="#" target="_blank">Fullstack PHP Developer</a>
                                    </p>

                                    <p>Walmart</p>
                                </td>

                            </tr>

                        </table>
                    </div>
                </div>
            </div>
            <div class="dashboard-footer mb-3">
                <a href="#" target="_blank">JobSite</a> Version 1.0.0
            </div>
        </div>
    </div>
@endsection