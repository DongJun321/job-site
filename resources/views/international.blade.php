@extends("layouts.theme")
@section('content')
    <!-- Categories Section Start-->
    <div class="home-categories-wrap bg-white pb-5 pt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="mb-3">Browse Category</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <p>
                        <a href="javascript:; class="category-link"><i class="la la-th-large"></i> Accounting/Finance <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Agro (Plant/Animal/Fisheries) <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:; class="category-link"><i class="la la-th-large"></i> Bank/ Non-Bank Fin. Institution <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Beauty Care/ Health &amp; Fitness <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:; class="category-link"><i class="la la-th-large"></i> Commercial/Supply Chain <span class="text-muted">(3)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Customer Support/Call Centre <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Data Entry/Operator/BPO <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Design/Creative <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Driving/Motor Technician <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:; class="category-link"><i class="la la-th-large"></i> Education/Training <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Electrician/ Construction/ Repair <span class="text-muted">(2)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:; class="category-link"><i class="la la-th-large"></i> Engineer/Architects <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:; class="category-link"><i class="la la-th-large"></i> Garments/Textile <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:; class="category-link"><i class="la la-th-large"></i> Gen Mgt/Admin <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Hospitality/ Travel/ Tourism <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:; class="category-link"><i class="la la-th-large"></i> HR/Org. Development <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> IT &amp; Telecommunication <span class="text-muted">(7)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Law/Legal <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Marketing/Sales <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Media/Ad./Event Mgt. <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Medical/Pharma <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> NGO/Development <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Others <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Production/Operation <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Research/Consultancy <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Secretary/Receptionist <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Security/Support Service <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- Categories Section End -->
@endsection