@extends('layouts.theme')
@section('content')
    <!-- Categories Section Start-->
    <div class="home-categories-wrap bg-white pb-5 pt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="mb-3">Browse Category</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <p>
                        <a href="javascript:; class="category-link"><i class="la la-th-large"></i> Accounting/Finance <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Agro (Plant/Animal/Fisheries) <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:; class="category-link"><i class="la la-th-large"></i> Bank/ Non-Bank Fin. Institution <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Beauty Care/ Health &amp; Fitness <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:; class="category-link"><i class="la la-th-large"></i> Commercial/Supply Chain <span class="text-muted">(3)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Customer Support/Call Centre <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Data Entry/Operator/BPO <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Design/Creative <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Driving/Motor Technician <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:; class="category-link"><i class="la la-th-large"></i> Education/Training <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Electrician/ Construction/ Repair <span class="text-muted">(2)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:; class="category-link"><i class="la la-th-large"></i> Engineer/Architects <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:; class="category-link"><i class="la la-th-large"></i> Garments/Textile <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:; class="category-link"><i class="la la-th-large"></i> Gen Mgt/Admin <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Hospitality/ Travel/ Tourism <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:; class="category-link"><i class="la la-th-large"></i> HR/Org. Development <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> IT &amp; Telecommunication <span class="text-muted">(7)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Law/Legal <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Marketing/Sales <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Media/Ad./Event Mgt. <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Medical/Pharma <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> NGO/Development <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Others <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Production/Operation <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Research/Consultancy <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Secretary/Receptionist <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>
                        <a href="javascript:;" class="category-link"><i class="la la-th-large"></i> Security/Support Service <span class="text-muted">(0)</span> </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- Categories Section End -->


    <section class="home-task-sec">
        <div class="container">
            <div class="row">
                <h2>See what others are getting done</h2>
                <ul class="tabs-custom-nav">
                    <li><a href="#tab01" class="current">Moving home</a></li>
                    <li><a href="#tab02">Starting a business</a></li>
                    <li><a href="#tab03">Fixing stuff</a></li>
                    <li><a href="#tab04">Hosting a party</a></li>
                    <li><a href="#tab05">Something different</a></li>
                </ul>
            </div>
        </div>
        <div class="tabs-custom general">
            <div id="tab01" class="selected tab-content-panel">
                <p class="tab-txt">Got a few boxes to shift, an apartment or entire house? Get your home moved just the way you want, by whom you want, when you want. Let Airtasker shoulder the load.</p>
                <div class="floating-tasks-container">
                    <div class="floating-tasks-container-wrapper">
                        <div class="home-task-container home-task-container-row-2">
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>


                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>


                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>


                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>


                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>


                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="floating-tasks-container">
                    <div class="floating-tasks-container-wrapper">
                        <div class="home-task-container home-task-container-align home-task-container-row-1">
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                        </div>
                    </div>
                </div>
            </div>


            <div id="tab02" class="tab-content-panel">
                <p class="tab-txt">Taking a big leap and need some expert advice or assistance? Airtasker can help you get some cracking marketing collateral, admin muscle or a few extra hands to help ease the burden.</p>
                <div class="floating-tasks-container">
                    <div class="floating-tasks-container-wrapper">
                        <div class="home-task-container home-task-container-row-1">
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>


                        </div>
                    </div>
                </div>
                <div class="floating-tasks-container">
                    <div class="floating-tasks-container-wrapper">
                        <div class="home-task-container home-task-container-align home-task-container-row-2">
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                        </div>
                    </div>
                </div>
            </div>


            <div id="tab03" class="tab-content-panel">
                <p class="tab-txt">Do you have a hole in the wall that needs plugging? Perhaps a TV that needs mounting? Or maybe you have that perfect shade of green, but no time to paint? Get a Tasker to help.</p>
                <div class="floating-tasks-container">
                    <div class="floating-tasks-container-wrapper">
                        <div class="home-task-container home-task-container-row-1">
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                        </div>
                    </div>
                </div>
                <div class="floating-tasks-container">
                    <div class="floating-tasks-container-wrapper">
                        <div class="home-task-container home-task-container-align home-task-container-row-2">
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>


                        </div>
                    </div>
                </div>
            </div>

            <div id="tab04" class="tab-content-panel">
                <p class="tab-txt">Got something to celebrate and the guest list all ready, but need everything else? Let Airtasker help you find the best bartenders, party planners, photographers and entertainment in the land.</p>
                <div class="floating-tasks-container">
                    <div class="floating-tasks-container-wrapper">
                        <div class="home-task-container home-task-container-row-1">
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                        </div>
                    </div>
                </div>
                <div class="floating-tasks-container">
                    <div class="floating-tasks-container-wrapper">
                        <div class="home-task-container home-task-container-align home-task-container-row-2">
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                        </div>
                    </div>
                </div>
            </div>

            <div id="tab05" class="tab-content-panel">
                <p class="tab-txt">Want something done but feel like it’s too random? Whether it’s getting rescued from a spider or help building a bobsled - you can get nearly anything done through Airtasker.</p>
                <div class="floating-tasks-container">
                    <div class="floating-tasks-container-wrapper">
                        <div class="home-task-container home-task-container-row-1">
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                        </div>
                    </div>
                </div>
                <div class="floating-tasks-container">
                    <div class="floating-tasks-container-wrapper">
                        <div class="home-task-container home-task-container-align home-task-container-row-2">
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>
                            <a class="life_moments_task_link" target="_blank">
                                <span class="life-spn">Delivery</span>
                                <div class="life_moments_task">
                                    <img src="https://eu7cmie.cloudimg.io/s/crop/64x64/https://assets-airtasker-com.s3.amazonaws.com/uploads/user/avatar/1973093/21192390_10155629195209120_3930992969904199411_n-a225529a26e1a7d2db1bc1a6686038a1.jpg" class="life_moments_task__TaskAvatar-sc-153tdrc-8 ewCZDU">
                                    <h5>Move Boxes and Pushbike from Kew to Sydney</h5>
                                    <p>$250</p>
                                </div>
                                <div class="ct-str-count-box">
                                    <svg height="16" width="16" class="life_moments" viewBox="0 0 24 24">
                                        <path d="M16.2 8.16l4.74.73a1.23 1.23 0 0 1 .67 2.11l-3.46 3.28a1.23 1.23 0 0 0-.37 1.1l.77 4.68a1.24 1.24 0 0 1-1.82 1.29L12.5 19.1a1.28 1.28 0 0 0-1.16 0l-4.27 2.17A1.25 1.25 0 0 1 5.27 20l.85-4.68a1.19 1.19 0 0 0-.34-1.09l-3.41-3.4a1.23 1.23 0 0 1 .71-2.1l4.75-.64a1.26 1.26 0 0 0 .95-.67l2.16-4.24a1.25 1.25 0 0 1 2.24 0l2.09 4.28a1.22 1.22 0 0 0 .93.7z"></path>
                                    </svg>
                                    <div class="ct-str-count">5 Stars</div>
                                </div>
                            </a>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>






    <!-- Blog Section Start-->
    <div class="home-blog-section pb-5 pt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="pricing-section-heading mb-5 text-center">
                        <h1>From Our Blog</h1>
                        <h5 class="text-muted">Check the latest updates/news from us.</h5>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="blog-card-wrap bg-white p-3 mb-4">
                        <div class="blog-card-img mb-4">
                            <img src="assets/images/pic01.jpg" class="card-img" />
                        </div>
                        <h4 class="mb-3">Job Seeker can track their application through dashboard</h4>
                        <p class="blog-card-text-preview">Lorem ipsum dolor sit amet, nibh propriae imperdiet ea vis, pro in epicuri appareat antiopam, odio vidit movet in nec. Quo diam noluisse adipisci ea, verear reprimique est an, ad ...</p>
                        <a href="javascript:;" class="btn btn-success"> <i class="la la-book"></i> Read More</a>
                        <div class="blog-card-footer border-top pt-3 mt-3">
                            <span><i class="la la-user"></i> John Doe </span>
                            <span><i class="la la-clock-o"></i> 1 year ago </span>
                            <span><i class="la la-eye"></i> 15 </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="blog-card-wrap bg-white p-3 mb-4">
                        <div class="blog-card-img mb-4">
                            <img src="assets/images/pic002.jpg" class="card-img" />
                        </div>
                        <h4 class="mb-3">JobFair is the best job board application</h4>
                        <p class="blog-card-text-preview">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even ...</p>
                        <a href="javascript:;" class="btn btn-success"> <i class="la la-book"></i> Read More</a>
                        <div class="blog-card-footer border-top pt-3 mt-3">
                            <span><i class="la la-user"></i> John Doe </span>
                            <span><i class="la la-clock-o"></i> 1 year ago </span>
                            <span><i class="la la-eye"></i> 5 </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="blog-card-wrap bg-white p-3 mb-4">
                        <div class="blog-card-img mb-4">
                            <img src="assets/images/pic003.jpg" class="card-img" />
                        </div>
                        <h4 class="mb-3">Adding post with feature image</h4>
                        <p class="blog-card-text-preview">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, ...</p>
                        <a href="javascript:;" class="btn btn-success"> <i class="la la-book"></i> Read More</a>
                        <div class="blog-card-footer border-top pt-3 mt-3">
                            <span><i class="la la-user"></i> John Doe </span>
                            <span><i class="la la-clock-o"></i> 1 year ago </span>
                            <span><i class="la la-eye"></i> 3 </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="home-all-blog-posts-btn-wrap text-center my-3">
                        <a href="" class="btn btn-success btn-lg"><i class="la la-link"></i> All Blog Posts</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Blog Section End-->
@endsection